// Simple postfix evaluator.

using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;

class PostfixEvaluator {
    public static void Main() {
        Console.Write("Give me a postfix expression: ");
        var input = Console.ReadLine();
        var regex = new Regex(@"(\d+)|([+])|([-])|(\s)|(.)");
        var stack = new Stack<int>();
        foreach (Match m in regex.Matches(input)) {
            if (m.Groups[1].Length > 0) {
                var x = Convert.ToInt32(m.Value); 
                stack.Push(x);
            } else if (m.Groups[2].Length > 0) {
                if (stack.Count < 2) {
                    Console.WriteLine("ERROR: Stack underflow!");
                    Environment.Exit(1);
                }
                var y = stack.Pop();
                var x = stack.Pop();
                var r = x + y;
                stack.Push(r);
            } else if (m.Groups[3].Length > 0) {
                if (stack.Count < 2) {
                    Console.WriteLine("ERROR: Stack underflow!");
                    Environment.Exit(1);
                }
                var y = stack.Pop();
                var x = stack.Pop();
                var r = x - y;
                stack.Push(r);
            } else if (m.Groups[4].Length > 0) {
                continue;
            } else {
                Console.WriteLine("ERROR: Bad symbol: " + m.Value);
                Environment.Exit(1);
            }
        }
        if (stack.Count != 1) {
            Console.WriteLine("ERROR: Too few operators");
            Environment.Exit(1);
        }
        var result = stack.Pop();
        Console.WriteLine(result);
    }
}