//---------------------------------------------------------
// Version 2 MBDLE.
//---------------------------------------------------------

%{

#include <stdio.h>
#include <stdarg.h>

int yylex(void);
void yyerror(char *s, ...);

%}

%union {
    int ival;
}

/* declare tokens */
%token ATOM ILLEGAL EOL LT_ROUND RT_ROUND LT_SQUARE RT_SQUARE
%token LT_ANGLE RT_ANGLE LT_CURLY RT_CURLY COMMA

%%

mbdle:
    /* nothing */ { }                              /* Matches at beginning of input */
    | mbdle expr EOL { printf("syntax ok\n> "); }  /* EOL is end of an expression */
;

expr:
    ATOM
    | LT_ROUND list RT_ROUND
    | LT_SQUARE list RT_SQUARE
    | LT_ANGLE list RT_ANGLE
    | LT_CURLY list RT_CURLY
;

list:
    /* nothing */
    | expr list_cont
;

list_cont:
    /* nothing */
    | COMMA expr list_cont
;

%%

int main(int argc, char **argv) {
    printf("> ");
    yyparse();
    return 0;
}

void yyerror(char *s, ...) {
    va_list ap;
    va_start(ap, s);
    vfprintf(stderr, s, ap);
    fprintf(stderr, "\n");
}
