/*---------------------------------------------------------
  Version 2 MBDLE.
-----------------------------------------------------------*/

%{

#include "mbdle_tokens.h"

%}

%%

[a-zA-Z0-9_]+  { return ATOM; }
"("            { return LT_ROUND; }
")"            { return RT_ROUND; }
"["            { return LT_SQUARE; }
"]"            { return RT_SQUARE; }
"<"            { return LT_ANGLE; }
">"            { return RT_ANGLE; }
"{"            { return LT_CURLY; }
"}"            { return RT_CURLY; }
","            { return COMMA; }
[ \t]          { /* ignore whitespace */ }
\n             { return EOL; }
.              { return ILLEGAL; }

%%
