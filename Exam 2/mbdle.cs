//---------------------------------------------------------
// Version 1 MBDLE.
//---------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

public enum TokenCategory {
    ATOM, EOL, ILLEGAL, LT_ROUND, RT_ROUND, LT_SQUARE, RT_SQUARE,
    LT_ANGLE, RT_ANGLE, LT_CURLY, RT_CURLY, COMMA
}

public class Token {

    TokenCategory category;

    public TokenCategory Category {
        get { return category; }
    }

    public Token(TokenCategory category) {
        this.category = category;
    }
}

public class Scanner {

    readonly String input;

    static readonly Regex regex =
        new Regex(
            @"                             
                (?<Atom>       \w+ )
              | (?<LtRound>    \(  )
              | (?<RtRound>    \)  )
              | (?<LtSquare>   \[  )
              | (?<RtSquare>   \]  )
              | (?<LtAngle>    \<  )
              | (?<RtAngle>    \>  )
              | (?<LtCurly>    \{  )
              | (?<RtCurly>    \}  )
              | (?<Comma>      \,  )
              | (?<Space>      \s  )
              | (?<Other>      .   )",
              RegexOptions.IgnorePatternWhitespace
            | RegexOptions.Compiled
            | RegexOptions.Multiline
        );
        
    static readonly IDictionary<string, TokenCategory> tokens =
            new Dictionary<string, TokenCategory>() {
                {"Atom",     TokenCategory.ATOM},
                {"LtRound",  TokenCategory.LT_ROUND},
                {"RtRound",  TokenCategory.RT_ROUND},
                {"LtSquare", TokenCategory.LT_SQUARE},
                {"RtSquare", TokenCategory.RT_SQUARE},
                {"LtAngle",  TokenCategory.LT_ANGLE},
                {"RtAngle",  TokenCategory.RT_ANGLE},
                {"LtCurly",  TokenCategory.LT_CURLY},
                {"RtCurly",  TokenCategory.RT_CURLY},
                {"Comma",    TokenCategory.COMMA},
                {"Other",    TokenCategory.ILLEGAL}
            };

    public Scanner(String input) {
        this.input = input;
    }

    public IEnumerable<Token> Start() {
        foreach (Match m in regex.Matches(input)) {
            if (m.Groups["Space"].Success) {
                continue;
            } else {
                foreach (var name in tokens.Keys) {
                    if (m.Groups[name].Success) {
                        yield return new Token(tokens[name]);
                        break;
                    }
                }
            }
        }
        yield return new Token(TokenCategory.EOL);
    }
}

class SyntaxError: Exception {
}

public class Parser {

    IEnumerator<Token> tokenStream;
    
    static readonly ISet<TokenCategory> firstOfExpr =
        new HashSet<TokenCategory>() {
            TokenCategory.ATOM,
            TokenCategory.LT_ROUND,
            TokenCategory.LT_SQUARE,
            TokenCategory.LT_ANGLE,
            TokenCategory.LT_CURLY
        };

    public Parser(IEnumerator<Token> tokenStream) {
        this.tokenStream = tokenStream;
        this.tokenStream.MoveNext();
    }

    public TokenCategory Current {
        get { return tokenStream.Current.Category; }
    }

    public Token Expect(TokenCategory category) {
        if (Current == category) {
            Token current = tokenStream.Current;
            tokenStream.MoveNext();
            return current;
        } else {
            throw new SyntaxError();
        }
    }

    public void Mbdle() {
        Expr();
        Expect(TokenCategory.EOL);
    }
    
    public void Expr() {
        switch (Current) {
            
        case TokenCategory.ATOM:
            Expect(TokenCategory.ATOM);
            break;
        
        case TokenCategory.LT_ROUND:
            Expect(TokenCategory.LT_ROUND);
            List();
            Expect(TokenCategory.RT_ROUND);
            break;
        
        case TokenCategory.LT_SQUARE:
            Expect(TokenCategory.LT_SQUARE);
            List();
            Expect(TokenCategory.RT_SQUARE);
            break;
        
        case TokenCategory.LT_ANGLE:
            Expect(TokenCategory.LT_ANGLE);
            List();
            Expect(TokenCategory.RT_ANGLE);
            break;
        
        case TokenCategory.LT_CURLY:
            Expect(TokenCategory.LT_CURLY);
            List();
            Expect(TokenCategory.RT_CURLY);
            break;
        
        default:
            throw new SyntaxError();
        }
    }
    
    public void List() {
        if (firstOfExpr.Contains(Current)) {
            Expr();
            while (Current == TokenCategory.COMMA) {
                Expect(TokenCategory.COMMA);
                Expr();
            }
        }
    }
}

public class MBDLE {

    public static void Main(String[] args) {
        try {
            while (true) {
                Console.Write("> ");
                var line = Console.ReadLine();
                if (line == null) {
                    break;
                }
                var parser = new Parser(new Scanner(line).Start().GetEnumerator());
                parser.Mbdle();
                Console.WriteLine("syntax ok");
            }
        } catch (SyntaxError) {
            Console.WriteLine("syntax error");
        }
    }
}
