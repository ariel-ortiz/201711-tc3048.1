class IfExample {
    public static void Main() {
        bool x = true;
        int y = 5;
        if (x) {
            y++;
        } else {
            y--;
        }
    }
}