﻿// Some examples using the dynamic keyword.

using System;

public class DynamicExample {

    static void m(int x) {
        Console.WriteLine("integer");
    }

    static void m(String s) {
        Console.WriteLine("string");
    }

	public static void Main() {
        /*
        dynamic a, b;
        a = 2;
        b = 5;
        Console.WriteLine(a + b);
        a = "One";
        b = "Two";
        Console.WriteLine(a + b);
        */
        dynamic x;
        x = 42;
        m(x);
        x = "hello";
        m(x);

        int y = 42;
        m((dynamic) y);
	}
}
